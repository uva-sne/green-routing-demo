package org.opennaas.gui.nfvrouting.controllers;

import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.lang.System;

import org.apache.log4j.Logger;
import org.opennaas.gui.nfvrouting.entities.settings.Settings;
import org.opennaas.gui.nfvrouting.entities.Route;
import org.opennaas.gui.nfvrouting.services.rest.RestServiceException;
import org.opennaas.gui.nfvrouting.services.rest.routing.NFVRoutingService;


public class NFVRoutingController {

    private static final Logger LOGGER = Logger.getLogger(NFVRoutingController.class); 

    public static void main(String[] args) throws RestServiceException, IOException {
    	String response = null; 
     	NFVRoutingService nfvRoutingService = new NFVRoutingService();
     	String mode = "dijkstra";
     	float time = 0;
         try{
             response = nfvRoutingService.setONRouteMode(mode);
    		 if (response.equals("OpenNaaS is not started")) {
    		     LOGGER.error("OpenNaaS is not started");
    		 }
    		 float start = System.currentTimeMillis();
    		 response = nfvRoutingService.getRoute("192.168.122.111", "192.168.121.202", "00:00:64:87:88:58:f6:57", "3");
    		 float end = System.currentTimeMillis();
    		 time = end-start;
         }catch (RestServiceException e) {
      	  LOGGER.error("show error message:");
      	  LOGGER.error(e.getMessage());
      	  throw e;
        }

        System.out.println(response); // Display the string.
        System.out.println(time);
        
    }
    

    
    
}