package org.opennaas.gui.nfvrouting.services.rest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import java.util.Locale;
import javax.ws.rs.core.Response.Status.Family;
import org.apache.log4j.Logger;
import org.opennaas.gui.nfvrouting.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Jordi
 */
public abstract class GenericRestService {

	private static final Logger LOGGER	= Logger.getLogger(GenericRestService.class);

	@Autowired
	private ReloadableResourceBundleMessageSource	configSource;

	@Autowired
	private ReloadableResourceBundleMessageSource	messageSource;

	/**
	 * @param path
	 * @return the url rest to call
	 * @throws RestServiceException 
	 */
	protected String getURL(String path) throws RestServiceException {
		String url = null;
		try{
		//url = configSource.getMessage(Constants.WS_REST_URL, null, Locale.getDefault()) + path;
		url = Constants.WS_REST_URL + path;
		LOGGER.info("Web service url: " + url);
		}
		catch (Exception e){
			throw new RestServiceException("get URL error");
		}
		return url;
	}

	/**
	 * Check if response code is between 200 and 299
	 * 
         * @param response
	 * @return true if response code is between 200 and 299
	 * @throws RestServiceException
	 */
	protected Boolean checkResponse(ClientResponse response) throws RestServiceException {
		LOGGER.info("Response: " + response);
		Family family = ClientResponse.Status.fromStatusCode(response.getStatus()).getFamily();
		if (family.equals(Family.SERVER_ERROR)) {
			String message = response.getEntity(String.class);
			throw new RestServiceException((message != null && !message.equals("")) ?
					message : messageSource.getMessage("message.error.notdetailmessage", null, null));
		} else if (!family.equals(Family.SUCCESSFUL)) {
			throw new RestServiceException(response.toString());
		}
		return true;
	}
        
         /**
         * Add HTTP Basic Authentication header to REST call using current Authentication object stored in Spring Security SecurityContextHolder
         * 
         * @param client
         */
        protected void addHTTPBasicAuthentication(Client client) {
                //Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                //client.addFilter(new HTTPBasicAuthFilter(authentication.getName(), (String) authentication.getCredentials()));
//        	String authString = "admin:123456";
//        	byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
//			String authStringEnc = new String(authEncBytes);    
        	client.addFilter(new HTTPBasicAuthFilter("admin", "123456"));

        }
}
