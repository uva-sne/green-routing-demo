package org.opennaas.extensions.edl.node.capability;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.Load;
import org.opennaas.extensions.edl.node.model.edl.Metric;
import org.opennaas.core.resources.ModelElementNotFoundException;
import org.opennaas.core.resources.capability.CapabilityException;
import org.opennaas.core.resources.capability.ICapability;

public interface IPowerSetupCapability extends ICapability{

	
	@Path("/node")
	@POST
	public void setNodeName(String nodeName);
	
	
//	@Path("/node")
//	@GET
//	public void getNodeName(String nodeName);
	
	@Path("/energysoure/{esname}")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public EnergySource getEnergySource(@PathParam("esname") String esName)
			throws ModelElementNotFoundException;
	
	@Path("/energysource/{esname}")
	@POST
	@Produces(MediaType.APPLICATION_XML)
	public String createEnergySource(@PathParam("esname") String esName) 
			throws ModelElementNotFoundException;
	
	@Path("/energysource/{esname}")
	@DELETE
	public void deleteEnergySource(@PathParam("esname") String esName) 
			throws ModelElementNotFoundException;
	
	@Path("/energysource/{esname}/price")
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public void setEnergySourcePrice
	(@PathParam("esname") String esName, float price)
			throws ModelElementNotFoundException;
	
	@Path("/energysource/{esname}/emission")
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public void setEnergySourceEmission(@PathParam("esname") String esName, 
			float emissionRate)
			throws ModelElementNotFoundException;
	
	
	@Path("/powermeter")
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public void setNodePowerMeter( String pduName, 
			 int numOfModule, int numOfPort) 
			throws ModelElementNotFoundException;
	

	@Path("/powermeter/driver")
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public void setPowerMeterDriver(String pduName,
			String driverAdds) 
			throws ModelElementNotFoundException;
	
	@Path("/outlet")
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public void setNodeOutlet(
			int moduleNum, int portNum) 
			throws ModelElementNotFoundException;
	
	@Path("/monitorlog/{name}")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Load getLoadMetric(@PathParam("name") String metricName)
			throws ModelElementNotFoundException;
	
	@Path("/monitorlog/{name}")
	@POST
	@Produces(MediaType.APPLICATION_XML)
	public void createMetric(@PathParam("name") String metricName) 
			throws ModelElementNotFoundException;
	
	@Path("/monitorlog/{name}")
	@DELETE
	public void deleteMetric(@PathParam("name") String metricName) 
			throws ModelElementNotFoundException;
	
	/**
	 * Say Hello
	 * 
	 * @throws CapabilityException
	 */
	@Path("/sayHello")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public String sayHello(@QueryParam("userName") String userName) throws CapabilityException;
}
