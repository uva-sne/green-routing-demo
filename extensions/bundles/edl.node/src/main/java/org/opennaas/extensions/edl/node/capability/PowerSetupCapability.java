package org.opennaas.extensions.edl.node.capability;

/*
 * #%L
 * OpenNaaS :: EDL :: NODE
 * %%
 * Copyright (C) 2007 - 2014 Fundació Privada i2CAT, Internet i Innovació a Catalunya
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opennaas.core.resources.ModelElementNotFoundException;
import org.opennaas.core.resources.action.IAction;
import org.opennaas.core.resources.action.IActionSet;
import org.opennaas.core.resources.capability.AbstractCapability;
import org.opennaas.core.resources.capability.CapabilityException;
import org.opennaas.core.resources.descriptor.CapabilityDescriptor;
import org.opennaas.extensions.edl.node.Activator;
import org.opennaas.extensions.edl.node.model.edl.Driver;
import org.opennaas.extensions.edl.node.model.edl.EDLNode;
import org.opennaas.extensions.edl.node.model.edl.EnergyConsumption;
import org.opennaas.extensions.edl.node.model.edl.EnergySource;
import org.opennaas.extensions.edl.node.model.edl.Load;
import org.opennaas.extensions.edl.node.model.edl.Metric;
import org.opennaas.extensions.edl.node.model.edl.MonitorLog;
import org.opennaas.extensions.edl.node.model.edl.Outlet;
import org.opennaas.extensions.edl.node.model.edl.PowerConsumption;
import org.opennaas.extensions.edl.node.model.edl.PowerFactor;
import org.opennaas.extensions.edl.node.model.edl.PowerMeter;
import org.opennaas.extensions.edl.node.model.edl.Unit;

public class PowerSetupCapability extends AbstractCapability 
	implements IPowerSetupCapability{

	public static String CAPABILITY_TYPE = "edl_node_setup";
	private String resourceId = "";
	Log log = LogFactory.getLog(PowerSetupCapability.class);
	

	public PowerSetupCapability(CapabilityDescriptor descriptor, String resourceId) {
		super(descriptor);
		this.resourceId = resourceId;
		log.debug("Instaniate a Power Monitor Elements Setup Capability.");
	}
	
	@Override
	public void activate() throws CapabilityException {
		registerService(Activator.getContext(), CAPABILITY_TYPE, getResourceType(), getResourceName(), IPowerSetupCapability.class.getName());
		super.activate();
	}

	@Override
	public void deactivate() throws CapabilityException {
		unregisterService();
		super.deactivate();
	}
	
	
	@Override
	public String getCapabilityName() {
		
		return CAPABILITY_TYPE;
	}

	@Override
	public EnergySource getEnergySource(String esName) 
			throws ModelElementNotFoundException {
		if (esName == null)
			throw new ModelElementNotFoundException("Missing energy source element: " + esName);
		
		for (EnergySource source :
			((EDLNode) resource.getModel()).getEdlUseEnergySource()){
			if (esName.equals(source.getEdlEnergyName())){
				return source;
			}
		}
		throw new ModelElementNotFoundException("Missing energy source element: " + esName);
	}

	@Override
	public String createEnergySource(String esName) throws ModelElementNotFoundException{
		EnergySource source = new EnergySource();
		source.setEdlEnergyName(esName);
		EDLNode node = (EDLNode) resource.getModel();
		if (node.getEdlUseEnergySource() == null){
			Set<EnergySource> sources = new LinkedHashSet<EnergySource>();
			sources.add(source);
			node.setEdlUseEnergySource(sources);
		}else{
			for (EnergySource s: node.getEdlUseEnergySource()){
				if (s.getEdlEnergyName().equals(source.getEdlEnergyName()))
					throw new ModelElementNotFoundException(
							"Use other names, conflict on the name of energy source: " + esName);
			}
			node.getEdlUseEnergySource().add(source);
		}
			
		return source.getEdlEnergyName();
	}

	@Override
	public void deleteEnergySource(String esName)
			throws ModelElementNotFoundException {
		EnergySource sourceDel = getEnergySource(esName);
		EDLNode node = (EDLNode) resource.getModel();
		node.getEdlUseEnergySource().remove(sourceDel); 
	}


	@Override
	public void setEnergySourcePrice(String esName, float price)
			throws ModelElementNotFoundException{
		EnergySource source = getEnergySource(esName);
		source.setEdlElectricityPrice(price);
	}

	@Override
	public void setEnergySourceEmission(String esName, float emissionRate) 
			throws ModelElementNotFoundException{
		EnergySource source = getEnergySource(esName);
		source.setEdlEmissionPerUnitofEnergy(emissionRate);
		
	}


	@Override
	public void setNodeName(String nodeName) {
		((EDLNode) resource.getModel()).setEdlNodename(nodeName);
	}

	@Override
	public void setNodePowerMeter(String pduName, int numOfModules, int numOfPorts)
			throws ModelElementNotFoundException {
		EDLNode node = (EDLNode) resource.getModel();
		PowerMeter pdu = new PowerMeter();
		pdu.setEdlPDUName(pduName);
		pdu.setEdlNumberOfModule(numOfModules);
		pdu.setEdlNumberOfOutlet(numOfPorts);
		node.setEdlMonitoredBy(pdu);
		
	}

	@Override
	public void setPowerMeterDriver(String pduName, String driverAdds)
			throws ModelElementNotFoundException {
		EDLNode node = (EDLNode) resource.getModel();
		Driver driver = new Driver();
		driver.setEdlURLaddress(driverAdds);
		node.getEdlMonitoredBy().setEdlHasDriver(driver);
		
	}

	@Override
	public void setNodeOutlet( int moduleNum, int portNum)
			throws ModelElementNotFoundException {
		EDLNode node = (EDLNode) resource.getModel();
		Outlet outlet = new Outlet();
		outlet.setEdlModulenumber(moduleNum);
		outlet.setEdlPortnumber(portNum);
		node.setEdlAttachTo(outlet);
		//node.getEdlMonitoredBy().getEdlHasOutlet().add(outlet);
		
	}


	
	@Override
	public Load getLoadMetric(String metric) throws ModelElementNotFoundException {
		EDLNode node = (EDLNode) resource.getModel();
		for (Load load : node.getEdlHasLog().getEdlIncludeLoad()){
			System.out.println(load.getEdlMaptoMetric().getEdlHasMetricName());
			if (load.getEdlMaptoMetric().getEdlHasMetricName().equals(metric)){
				return load;
			}
		}
		throw new ModelElementNotFoundException("Missing metric element: " + metric);
	}
	
	@Override
	public void createMetric(String metric)
			throws ModelElementNotFoundException {
		EDLNode node = (EDLNode) resource.getModel();
		MonitorLog log = null;
		if (node.getEdlHasLog() == null){
			log = new MonitorLog();
		}else {
			log = node.getEdlHasLog();
		}
		// initialize the default metrics if without metric
		if (log.getEdlIncludeLoad() == null){
			Load powerLoad = new Load();
			PowerConsumption powerMetric = new PowerConsumption();
			powerMetric.setEdlHasMetricName("PowerConsumption");
			Unit powerUnit = new Unit();
			powerUnit.setEdlUnitname("Watt");
			powerMetric.setEdlHasUnit(powerUnit);
			powerLoad.setEdlMaptoMetric(powerMetric);
			
			Load energyLoad = new Load();
			EnergyConsumption energyMetric = new EnergyConsumption();
			energyMetric.setEdlHasMetricName("EnergyConsumption");
			Unit energyUnit = new Unit();
			energyUnit.setEdlUnitname("Wh");
			energyMetric.setEdlHasUnit(energyUnit);
			energyLoad.setEdlMaptoMetric(energyMetric);
			
			Load pfLoad = new Load();
			PowerFactor pfMetric = new PowerFactor();
			pfMetric.setEdlHasMetricName("PowerFactor");
			Unit pfUnit = new Unit();
			pfUnit.setEdlUnitname("%");
			pfMetric.setEdlHasUnit(pfUnit);
			pfLoad.setEdlMaptoMetric(pfMetric);
			
			Set<Load> loads = new LinkedHashSet<Load>();
			loads.add(powerLoad);
			loads.add(energyLoad);
			loads.add(pfLoad);
			log.setEdlIncludeLoad(loads);
			node.setEdlHasLog(log);
		}

		for (Load load: node.getEdlHasLog().getEdlIncludeLoad()){
			if (load.getEdlMaptoMetric().getEdlHasMetricName().equals(metric))
				throw new ModelElementNotFoundException(
						"Use other metrics, the metric already exists: " + metric);
		}
		
		Load targetLoad = new Load();
		Metric targetMetric = new Metric();
		targetMetric.setEdlHasMetricName(metric);
		Unit targetUnit = new Unit();
		// the default units of metrics
		if(metric.equals("EnergyEfficiency"))
			targetUnit.setEdlUnitname("Bytes/Joule");
		else if(metric.equals("EmissionEfficiency"))
			targetUnit.setEdlUnitname("Ton/Joule");
		else if(metric.equals("TotalEmission"))
			targetUnit.setEdlUnitname("Ton");
		else if(metric.equals("TotalElectricityCost"))
			targetUnit.setEdlUnitname("Euro");

		targetMetric.setEdlHasUnit(targetUnit);
		targetLoad.setEdlMaptoMetric(targetMetric);
		log.getEdlIncludeLoad().add(targetLoad);
		node.setEdlHasLog(log);
	}
	
	
	@Override
	public void deleteMetric(String metric)
			throws ModelElementNotFoundException {
		EDLNode node = (EDLNode) resource.getModel();
		Load loadDel = getLoadMetric(metric);
		MonitorLog log = node.getEdlHasLog();
		log.getEdlIncludeLoad().remove(loadDel);
		node.setEdlHasLog(log);
	}
	
	
	@Override
	public void queueAction(IAction action) throws CapabilityException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IActionSet getActionSet() throws CapabilityException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param
	 *  the user name
	 * @return the greeting message
	 * 
	 */
	@Override
	public String sayHello(String userName) throws CapabilityException {
		return "Hello " + userName;
	}





}
