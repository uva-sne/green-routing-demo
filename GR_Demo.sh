resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/edlnode1.descriptor
resource:start edlnode:node1
edl:setNodePowerMeter edlnode:node1 rpdu-vu01 3 8
edl:setNodeOutlet edlnode:node1 3 0
edl:setPowerMeterDriver edlnode:node1 rpdu-vu01 localhost/16120
edl:createEnergySource edlnode:node1 solar1
edl:setEnergySourceEmission edlnode:node1 solar1 0.015
edl:setEnergySourcePrice edlnode:node1 solar1 0.42
edl:show edlnode:node1


resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/edlnode2.descriptor
resource:start edlnode:node2
edl:setNodePowerMeter edlnode:node2 rpdu-vu01 3 8
edl:setNodeOutlet edlnode:node2 1 7
edl:setPowerMeterDriver edlnode:node2 rpdu-vu01 localhost/16120
edl:createEnergySource edlnode:node2 solar1
edl:setEnergySourceEmission edlnode:node2 solar1 0.015
edl:setEnergySourcePrice edlnode:node2 solar1 0.42

edl:show edlnode:node2


resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/edlnode3.descriptor
resource:start edlnode:node3
edl:setNodePowerMeter edlnode:node3 rpdu-vu01 3 8
edl:setNodeOutlet edlnode:node3 3 2
edl:setPowerMeterDriver edlnode:node3 rpdu-vu01 localhost/16120
edl:createEnergySource edlnode:node3 thermal1
edl:setEnergySourceEmission edlnode:node3 thermal1 0.045
edl:setEnergySourcePrice edlnode:node3 thermal1 0.98

edl:show edlnode:node3


resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/edlnode4.descriptor
resource:start edlnode:node4
edl:setNodePowerMeter edlnode:node4 rpdu-vu01 3 8
edl:setNodeOutlet edlnode:node4 1 1
edl:setPowerMeterDriver edlnode:node4 rpdu-vu01 localhost/16120
edl:createEnergySource edlnode:node4 thermal1
edl:setEnergySourceEmission edlnode:node4 thermal1 0.045
edl:setEnergySourcePrice edlnode:node4 thermal1 0.98
edl:show edlnode:node4

resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/edlnode5.descriptor
resource:start edlnode:node5
edl:setNodePowerMeter edlnode:node5 rpdu-vu01 3 8
edl:setNodeOutlet edlnode:node5 3 3
edl:setPowerMeterDriver edlnode:node5 rpdu-vu01 localhost/16120
edl:createEnergySource edlnode:node5 thermal1
edl:setEnergySourceEmission edlnode:node5 thermal1 0.045
edl:setEnergySourcePrice edlnode:node5 thermal1 0.98
edl:show edlnode:node5

resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/edlnode6.descriptor
resource:start edlnode:node6
edl:setNodePowerMeter edlnode:node6 rpdu-vu01 3 8
edl:setNodeOutlet edlnode:node6 3 1
edl:setPowerMeterDriver edlnode:node6 rpdu-vu01 localhost/16120
edl:createEnergySource edlnode:node6 solar1
edl:setEnergySourceEmission edlnode:node6 solar1 0.015
edl:setEnergySourcePrice edlnode:node6 solar1 0.42
edl:show edlnode:node6



resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/SW1-fdl.descriptor
protocols:context -p protocol.floodlight.switchid=00:00:64:87:88:58:f6:57 openflowswitch:SW1 floodlight noauth http://controllersVM1:8080
resource:start openflowswitch:SW1
edl:getCurrentObMeasurement openflowswitch:SW1

resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/SW2-fdl.descriptor
protocols:context -p protocol.floodlight.switchid=00:00:64:87:88:58:f8:57 openflowswitch:SW2 floodlight noauth http://controllersVM1:8080
resource:start openflowswitch:SW2
edl:getCurrentObMeasurement openflowswitch:SW2

resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/SW3-odl.descriptor
protocols:context -p protocol.opendaylight.switchid=00:00:00:00:00:00:00:03 openflowswitch:SW3 opendaylight noauth http://controllersVM2:8080
resource:start openflowswitch:SW3
edl:getCurrentObMeasurement openflowswitch:SW3

resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/SW4-odl.descriptor
protocols:context -p protocol.opendaylight.switchid=00:00:00:00:00:00:00:04 openflowswitch:SW4 opendaylight noauth http://controllersVM2:8080
resource:start openflowswitch:SW4
edl:getCurrentObMeasurement openflowswitch:SW4

resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/SW5-odl.descriptor
protocols:context -p protocol.opendaylight.switchid=00:00:00:00:00:00:00:05 openflowswitch:SW5 opendaylight noauth http://controllersVM2:8080
resource:start openflowswitch:SW5
edl:getCurrentObMeasurement openflowswitch:SW5


resource:create /opt/opennaas-routing-nfv-master/utils/examples/descriptors/SW6-fdl.descriptor
protocols:context -p protocol.floodlight.switchid=00:00:64:87:88:58:f8:77 openflowswitch:SW6 floodlight noauth http://controllersVM1:8080
resource:start openflowswitch:SW6
edl:getCurrentObMeasurement openflowswitch:SW6

